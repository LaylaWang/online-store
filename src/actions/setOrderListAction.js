import { fetchData } from '../utils/fetchData';

const setOrderList = () => dispatch => {
  fetchData('/orders')
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: 'SET_ORDER_LIST',
        orderList: res
      });
    });
};

export { setOrderList };