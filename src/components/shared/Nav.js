import React from 'react';
import { NavLink } from 'react-router-dom';
import { MdHome } from 'react-icons/md';
import { MdShoppingCart } from 'react-icons/md';
import { MdAdd } from 'react-icons/md';

import './Nav.less';

const Nav = () => {
  return (
    <nav>
      <ul className="nav">
        <li className="nav-item">
          <NavLink exact to="/" className="nav-link" activeClassName="selected">
            <div>
              <MdHome className="icon"/>
            </div>
            <div>商城</div>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/order" className="nav-link" activeClassName="selected">
            <div>
              <MdShoppingCart className="icon"/>
            </div>
            <div>订单</div>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/create-commodity" className="nav-link" activeClassName="selected">
            <div>
              <MdAdd className="icon"/>
            </div>
            <div>添加商品</div>
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
