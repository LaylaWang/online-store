import React, { Component } from 'react';
import './Button.less';

class Button extends Component {
  constructor(props) {
    super(props);
    this.handelClick = this.handelClick.bind(this);
  }

  handelClick() {
    this.props.onClick();
  }

  render() {
    return (
      <button
        className={`button ${this.props.color} ${this.props.shape} ${this.props.textColor}`}
        onClick={this.handelClick}
        type={this.props.type || 'button'}
        disabled={this.props.disabled || false}
      >
        {this.props.buttonText}
      </button>
    );
  }
}

export default Button;
