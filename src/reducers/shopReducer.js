const initState = {
  commodityList: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'SET_COMMODITY_LIST':
      return {
        ...state,
        commodityList: action.commodityList
      };

    default:
      return state;
  }
};
