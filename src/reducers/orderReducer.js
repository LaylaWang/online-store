const initState = {
  orderList: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'SET_ORDER_LIST':
      return {
        ...state,
        orderList: action.orderList
      };

    default:
      return state;
  }
};
